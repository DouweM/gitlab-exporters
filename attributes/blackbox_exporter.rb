#
# Cookbook Name GitLab::Monitoring
# Attributes:: blackbox_exporter
#

default["blackbox_exporter"]["dir"]         = "/opt/prometheus/blackbox_exporter"
default["blackbox_exporter"]["binary"]      = "#{node['blackbox_exporter']['dir']}/blackbox_exporter"
default["blackbox_exporter"]["log_dir"]     = "/var/log/prometheus/blackbox_exporter"

default["blackbox_exporter"]["version"]     = "0.11.0"
default["blackbox_exporter"]["checksum"]    = "6731efeb27d667ff5da362cc1afbb0a983d016697d906bb4b9c5503699efcbb7"
default["blackbox_exporter"]["binary_url"]  = "https://github.com/prometheus/blackbox_exporter/releases/download/v#{node['blackbox_exporter']['version']}/blackbox_exporter-#{node['blackbox_exporter']['version']}.linux-amd64.tar.gz"

default["blackbox_exporter"]["flags"]["config.file"]        = "#{node['blackbox_exporter']['dir']}/blackbox_exporter.yml"
default["blackbox_exporter"]["flags"]["web.listen-address"] = "0.0.0.0:9115"

default["blackbox_exporter"]["modules"]["http_2xx"]["prober"]         = "http"

default["blackbox_exporter"]["modules"]["http_post_2xx"]["prober"]    = "http"
default["blackbox_exporter"]["modules"]["http_post_2xx"]["http_method"] = "POST"

default["blackbox_exporter"]["modules"]["http_dev_gitlab_org_2xx"]["prober"] = "http"
default["blackbox_exporter"]["modules"]["http_dev_gitlab_org_2xx"]["http_method"] = "GET"

default["blackbox_exporter"]["modules"]["http_gitlab_com_auth_2xx"]["prober"] = "http"
default["blackbox_exporter"]["modules"]["http_gitlab_com_auth_2xx"]["http_method"] = "GET"

default["blackbox_exporter"]["modules"]["tcp_connect"]["prober"]        = "tcp"
default["blackbox_exporter"]["modules"]["tcp_connect"]["timeout"]       = "5s"

default["blackbox_exporter"]["modules"]["pop3s_banner"]["prober"]       = "tcp"
default["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_query_response"] = { "expect" => "^+OK" }
default["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_tls"] = "true"
default["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_tls_config"] = { "insecure_skip_verify" => "false" }
default["blackbox_exporter"]["modules"]["icmp"]["prober"]               = "icmp"
default["blackbox_exporter"]["modules"]["icmp"]["timeout"]              = "5s"
