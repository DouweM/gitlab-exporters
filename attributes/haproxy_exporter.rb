default["haproxy_exporter"]["checksum"]    = "2b1da4218fc5a1531ed17663ba5656c6bb5ce3db0ad6c2bdd6781d7f1b545816"
default["haproxy_exporter"]["dir"]         = "/opt/prometheus/haproxy_exporter"
default["haproxy_exporter"]["binary"]      = "#{node['haproxy_exporter']['dir']}/haproxy_exporter"
default["haproxy_exporter"]["log_dir"]     = "/var/log/prometheus/haproxy_exporter"
default["haproxy_exporter"]["version"]     = "0.8.0"
default["haproxy_exporter"]["binary_url"]  = "https://github.com/prometheus/haproxy_exporter/releases/download/v#{node['haproxy_exporter']['version']}/haproxy_exporter-#{node['haproxy_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["haproxy_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/haproxy_exporter
default["haproxy_exporter"]["flags"]["haproxy.scrape-uri"] = "unix:/run/haproxy/admin.sock"
default["haproxy_exporter"]["flags"]["haproxy.pid-file"]   = "/run/haproxy.pid"
