default["node_exporter"]["checksum"]    = "1ce667467e442d1f7fbfa7de29a8ffc3a7a0c84d24d7c695cc88b29e0752df37"
default["node_exporter"]["dir"]         = "/opt/prometheus/node_exporter"
default["node_exporter"]["scripts_dir"] = "#{node['node_exporter']['dir']}/scripts"
default["node_exporter"]["binary"]      = "#{node['node_exporter']['dir']}/node_exporter"
default["node_exporter"]["log_dir"]     = "/var/log/prometheus/node_exporter"
default["node_exporter"]["version"]     = "0.15.2"
default["node_exporter"]["binary_url"]  = "https://github.com/prometheus/node_exporter/releases/download/v#{node['node_exporter']['version']}/node_exporter-#{node['node_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["node_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/node_exporter#collectors

default["node_exporter"]["flags"]["web.listen-address"] = "0.0.0.0:9100"

# Enable extra collectors.
default["node_exporter"]["flags"]["collector.nfs"]        = true
default["node_exporter"]["flags"]["collector.ntp"]        = true

# Extra collector flags.
default["node_exporter"]["flags"]["collector.textfile.directory"] = "#{node['node_exporter']['dir']}/metrics"
default["node_exporter"]["flags"]["collector.filesystem.ignored-fs-types"] = "\"^(nfs.|fuse.lxcfs|rpc_pipefs|(sys|proc|auto)fs)$\""

default["node_exporter"]["ntpd_metrics_logdir"] = "/var/log/prometheus/node_exporter_ntpd_metrics"
