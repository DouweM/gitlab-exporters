#
# Cookbook Name GitLab::Monitoring
# Attributes:: postgres_exporter
#
default["postgres_exporter"]["dir"] = "/opt/prometheus/postgres_exporter"
default["postgres_exporter"]["log_dir"] = "/var/log/prometheus/postgres_exporter"
default["postgres_exporter"]["checksum"] = "219c2c116cb496d54ddbd23f392a38c3496ab8e7118dfbf8b7c0b21593dedbfd"
default["postgres_exporter"]["version"] = "0.4.1"
default["postgres_exporter"]["db_user"] = "postgres_exporter"
default["postgres_exporter"]["binary_url"] = "https://github.com/wrouesnel/postgres_exporter/releases/download/v#{node['postgres_exporter']['version']}/postgres_exporter_v#{node['postgres_exporter']['version']}_linux-amd64.tar.gz"

default["postgres_exporter"]["flags"]["extend.query-path"] = "#{node['postgres_exporter']['dir']}/queries.yaml"
