
default["redis_exporter"]["dir"] = "/opt/prometheus/redis_exporter"
default["redis_exporter"]["log_dir"] = "/var/log/prometheus/redis_exporter"
default["redis_exporter"]["version"] = "0.14"
default["redis_exporter"]["binary_url"] = "https://github.com/oliver006/redis_exporter/releases/download/v#{node['redis_exporter']['version']}/redis_exporter-v#{node['redis_exporter']['version']}.linux-amd64.tar.gz"
default["redis_exporter"]["checksum"] = "ba80d6902a7020ca94156741cb1d6e44480d19bfa753315d5e6b0f099921bce0"
default["redis_exporter"]["chef_vault"] = "gitlab-cluster-base"
default["redis_exporter"]["chef_vault_item"] = "_default"
