#!/usr/bin/env ruby

class AptAutoremoveMetrics
  def print_metrics
    print_packages_metrics 'upgraded', :upgraded
    print_packages_metrics 'newly installed', :newly_installed
    print_packages_metrics 'to remove', :to_remove
    print_packages_metrics 'not upgraded', :not_upgraded
    print_freed_space_metrics
  end

  private

  def apt_ar_output
    @apt_ar_output ||= `echo "n" | apt-get autoremove | grep -E -e "[0-9]+ upgraded" -e "After this operation, [0-9,]+ [A-Z]*B disk space"`
  end

  def print_packages_metrics(pattern, label)
    count = apt_ar_output.match(Regexp.new("([0-9]+) #{pattern}"))[1]

    puts 'apt_get_autoremove_packages{operation="%s"} %f' % [label, count]
  end

  def print_freed_space_metrics
    match = apt_ar_output.match(Regexp.new('([0-9,]+) ([kmg]*B) disk space will be freed', Regexp::IGNORECASE))
    size =  match[1].gsub(',', '').to_i
    factor = match[2].match(Regexp.new("([kmg])?B", Regexp::IGNORECASE))[1]
    bytes = case factor.downcase
              when 'k'
                size * 1024
              when 'm'
                size * 1024 * 1024
              when 'g'
                size * 1024 * 1024 * 1024
              else
                size
            end

    puts 'apt_get_autoremove_bytes{type="freed"} %f' % [bytes]
  end
end

m = AptAutoremoveMetrics.new
m.print_metrics
