# Depends on a node_exporter recipe.
include_recipe 'gitlab-exporters::node_exporter'

apt_get_autoremove_metrics_script = File.join(node['node_exporter']['scripts_dir'], 'apt_get_autoremove_metrics.rb')
cookbook_file apt_get_autoremove_metrics_script do
  source 'apt_get_autoremove_metrics.rb'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  only_if { node['platform_family'] == 'debian' }
end

apt_get_autoremove_textfile = File.join(node['node_exporter']['flags']['collector.textfile.directory'], 'apt_get_autoremove.prom')
cron 'measure_apt_get_autoremove' do
  minute '0'
  hour '0'
  command "#{apt_get_autoremove_metrics_script} > #{apt_get_autoremove_textfile}"
  only_if { node['platform_family'] == 'debian' }
end
