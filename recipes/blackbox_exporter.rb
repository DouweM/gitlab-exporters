require "yaml"
include_recipe "gitlab-vault"
include_recipe "gitlab-exporters::default"

# Fetch secrets from Chef Vault
blackbox_conf = GitLab::Vault.get(node, "gitlab-exporters", "blackbox_exporter")

directory node["blackbox_exporter"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory node["blackbox_exporter"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

include_recipe "ark::default"

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node["blackbox_exporter"]["dir"])
dir_path = ::File.dirname(node["blackbox_exporter"]["dir"])

ark dir_name do
  url node["blackbox_exporter"]["binary_url"]
  checksum node["blackbox_exporter"]["checksum"]
  version node["blackbox_exporter"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
end

template node["blackbox_exporter"]["flags"]["config.file"] do
  source "blackbox_exporter.yml.erb"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
end

blackbox_module "http_2xx" do
  prober node["blackbox_exporter"]["modules"]["http_2xx"]["prober"]
  http_method node["blackbox_exporter"]["modules"]["http_2xx"]["http_method"]
end

blackbox_module "http_post_2xx" do
  prober node["blackbox_exporter"]["modules"]["http_post_2xx"]["prober"]
  http_method node["blackbox_exporter"]["modules"]["http_post_2xx"]["http_method"]
end

blackbox_module "http_dev_gitlab_org_2xx" do
  prober node["blackbox_exporter"]["modules"]["http_dev_gitlab_org_2xx"]["prober"]
  http_method node["blackbox_exporter"]["modules"]["http_dev_gitlab_org_2xx"]["http_method"]
  auth_token blackbox_conf["dev_auth_2xx"]
end

blackbox_module "http_gitlab_com_auth_2xx" do
  prober node["blackbox_exporter"]["modules"]["http_gitlab_com_auth_2xx"]["prober"]
  http_method node["blackbox_exporter"]["modules"]["http_gitlab_com_auth_2xx"]["http_method"]
  auth_token blackbox_conf["prod_auth_2xx"]
end

blackbox_module "tcp_connect" do
  prober node["blackbox_exporter"]["modules"]["tcp_connect"]["prober"]
  timeout node["blackbox_exporter"]["modules"]["tcp_connect"]["timeout"]
end

blackbox_module "pop3s_banner" do
  prober node["blackbox_exporter"]["modules"]["pop3s_banner"]["prober"]
  tcp_tls node["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_tls"]
  tcp_tls_config node["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_tls_config"]
  tcp_query_response node["blackbox_exporter"]["modules"]["pop3s_banner"]["tcp_query_response"]
end

blackbox_module "icmp" do
  prober node["blackbox_exporter"]["modules"]["icmp"]["prober"]
  timeout node["blackbox_exporter"]["modules"]["icmp"]["timeout"]
end

accumulator node["blackbox_exporter"]["flags"]["config.file"] do
  filter        do |res| res.is_a? Chef::Resource::GitlabExportersBlackboxModule end
  target        template: node["blackbox_exporter"]["flags"]["config.file"]
  transform     do |modules| modules.sort_by(&:name) end
  variable_name :modules
  notifies      :restart, "service[blackbox_exporter]"
end

include_recipe "runit::default"
runit_service "blackbox_exporter" do
  default_logger true
  log_dir node["blackbox_exporter"]["log_dir"]
end
