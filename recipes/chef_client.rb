# Depends on a node_exporter recipe.
include_recipe "gitlab-exporters::node_exporter"

chef_gem "prometheus-client" do
  compile_time true
end

# `run_action` causes this to be executed in the first phase of the chef-client
# run, so that the handler is installed as soon as possible.
directory(node["chef_handler"]["handler_path"]) {
  group "root"
  owner "root"
  mode "0755"
  recursive true
  action :nothing
}.run_action(:create)

handler = "prometheus_handler.rb"
path    = File.join(node["chef_handler"]["handler_path"], handler)
cookbook_file(path) {
  source handler
  mode "0755"
  action :nothing
}.run_action(:create)

textfile = File.join(node["node_exporter"]["flags"]["collector.textfile.directory"], "chef-client.prom")
chef_handler("PrometheusHandler") {
  source path
  arguments textfile: textfile
  action :nothing
}.run_action(:enable)
