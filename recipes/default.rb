#
# Cookbook Name:: gitlab-monitoring
# Recipe:: default
#
# Copyright 2016, GitLab
#
# All rights reserved - Do Not Redistribute
#

user node["prometheus"]["user"] do
  system true
  shell "/bin/false"
  home "/opt/#{node['prometheus']['user']}"
  not_if node["prometheus"]["user"] == "root"
end

directory "/var/log/prometheus" do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end
