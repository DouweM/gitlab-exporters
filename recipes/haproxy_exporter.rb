require "yaml"
include_recipe "gitlab-exporters::default"

directory node["haproxy_exporter"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

# directory "#{node["haproxy_exporter"]["dir"]}/metrics" do
#   owner node["prometheus"]["user"]
#   group node["prometheus"]["group"]
#   mode "0755"
#   recursive true
# end

directory node["haproxy_exporter"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

include_recipe "ark::default"

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node["haproxy_exporter"]["dir"])
dir_path = ::File.dirname(node["haproxy_exporter"]["dir"])

ark dir_name do
  url node["haproxy_exporter"]["binary_url"]
  checksum node["haproxy_exporter"]["checksum"]
  version node["haproxy_exporter"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
end

include_recipe "runit::default"
runit_service "haproxy_exporter" do
  default_logger true
  log_dir node["haproxy_exporter"]["log_dir"]
end
