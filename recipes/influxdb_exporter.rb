require "yaml"
include_recipe "gitlab-exporters::default"
include_recipe "gitlab-exporters::chef_client"

directory node["influxdb_exporter"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory node["influxdb_exporter"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

include_recipe "ark::default"

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node["influxdb_exporter"]["dir"])
dir_path = ::File.dirname(node["influxdb_exporter"]["dir"])

ark dir_name do
  url node["influxdb_exporter"]["binary_url"]
  checksum node["influxdb_exporter"]["checksum"]
  version node["influxdb_exporter"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
end

include_recipe "runit::default"
runit_service "influxdb_exporter" do
  default_logger true
  log_dir node["influxdb_exporter"]["log_dir"]
end
