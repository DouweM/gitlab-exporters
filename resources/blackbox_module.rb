resource_name :blackbox_module
default_action :create
actions :delete

attribute :name,                kind_of: String, name_attribute: true, required: true
attribute :prober,              kind_of: String, required: true
attribute :timeout,             kind_of: String, default: "59s"
attribute :http_method,         kind_of: String
attribute :auth_token,          kind_of: String
attribute :tcp_query_response,  kind_of: Hash
attribute :tcp_tls_config,      kind_of: Hash
attribute :tcp_tls,             kind_of: String, default: "false"

action :create do
end

action :delete do
end
