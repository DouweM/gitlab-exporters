require 'spec_helper'

describe 'gitlab-exporters::apt_get_autoremove_exporter' do
  context 'on Debian platform' do
    cached(:chef_run_ubuntu) do
      ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
    end

    it 'creates the apt_get_autoremove_metrics.rb file' do
      expect(chef_run_ubuntu).to create_cookbook_file('/opt/prometheus/node_exporter/scripts/apt_get_autoremove_metrics.rb').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755'
      )
    end

    it 'creates cron entry' do
      expect(chef_run_ubuntu).to create_cron('measure_apt_get_autoremove').with(minute: '0', hour: '0')
    end
  end

  context 'on other platform' do
    cached(:chef_run_centos) do
      ChefSpec::SoloRunner.new(platform: 'fedora', version: '27').converge(described_recipe)
    end

    it "doesn't create the apt_get_autoremove_metrics.rb file" do
      expect(chef_run_centos).not_to create_cookbook_file('/opt/prometheus/node_exporter/apt_get_autoremove_metrics.rb')
    end

    it "doesn't create cron entry" do
      expect(chef_run_centos).not_to create_cron('measure_apt_get_autoremove')
    end
  end
end