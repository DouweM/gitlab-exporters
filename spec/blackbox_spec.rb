require "spec_helper"
require "chef-vault"

describe "gitlab-exporters::blackbox_exporter" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal["gitlab-exporters"]["blackbox_exporter"]["chef_vault"] = "blackbox-exporter"
      }.converge(described_recipe)
    end

    before do
      allow(Chef::DataBag).to receive(:load).with("blackbox-exporter").and_return("_default_keys" => { "id" => "_default_keys" })
      allow(ChefVault::Item).to receive(:load).with("blackbox-exporter", "_default").and_return("id" => "_default",
                                                                                                "gitlab-exporters" => {
                                                                                                  "blackbox_exporter" => {
                                                                                                    "token" => "test-token",
                                                                                                  },
                                                                                                })
    end

    it "creates the blackbox_exporter dir in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/blackbox_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/blackbox_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the configuration file with default content" do
      expect(chef_run).to create_template("/opt/prometheus/blackbox_exporter/blackbox_exporter.yml").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0644"
      )
      expect(chef_run).to render_file("/opt/prometheus/blackbox_exporter/blackbox_exporter.yml").with_content { |content|
        expect(content).to include("modules:")
      }
    end
  end
end
