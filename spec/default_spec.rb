require "spec_helper"

describe "gitlab-exporters::default" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates a prometheus user and group" do
      expect(chef_run).to create_user("prometheus").with(
        system: true,
        shell: "/bin/false"
      )
    end

    it "creates the base log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end
  end
end
