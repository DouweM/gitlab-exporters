require "spec_helper"

describe "gitlab-exporters::influxdb_exporter" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the prometheus dir in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/influxdb_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/influxdb_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("influxdb_exporter")
    end
  end
end
