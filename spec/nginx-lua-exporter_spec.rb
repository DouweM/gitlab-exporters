require "spec_helper"
require "chef-vault"
require "chef-vault/test_fixtures"

describe "gitlab-exporters::nginx-lua-exporter" do
  include ChefVault::TestFixtures.rspec_shared_context

  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/etc/nginx/lua").with(
        owner: "root",
        group: "root",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the lua script" do
      expect(chef_run).to create_cookbook_file("/etc/nginx/lua/prometheus.lua").with(
        owner: "root",
        group: "root",
        mode: "0644"
      )
    end

    it "creates the nginx site config" do
      expect(chef_run).to create_template("/etc/nginx/sites-available/nginx_lua_exporter").with(
        owner: "root",
        group: "root",
        mode: "0644"
      )
    end
  end
end
