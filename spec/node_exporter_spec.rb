require "spec_helper"

describe "gitlab-exporters::node_exporter" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the prometheus dir in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/node_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/node_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("node_exporter")
    end

    it 'creates scripts directory' do
      expect(chef_run).to create_directory('/opt/prometheus/node_exporter/scripts').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755'
      )
    end
  end

  context "ntpd metrics" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/node_exporter_ntpd_metrics").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("ntpd_metrics")
    end
  end
end
