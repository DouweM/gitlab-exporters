require "spec_helper"
require "chef-vault"
require "chef-vault/test_fixtures"

describe "gitlab-exporters::postgres_exporter" do
  include ChefVault::TestFixtures.rspec_shared_context

  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the prometheus dir and plugin in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/postgres_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the query configuration file" do
      expect(chef_run).to create_cookbook_file("/opt/prometheus/postgres_exporter/queries.yaml").with(
        source: "postgres_exporter/queries.yaml",
        owner: "prometheus",
        group: "prometheus",
        mode: "0644"
      )
      expect(chef_run.cookbook_file("/opt/prometheus/postgres_exporter/queries.yaml")).to notify("runit_service[postgres_exporter]").to(:reload).delayed
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/postgres_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("postgres_exporter")
    end
  end
end
