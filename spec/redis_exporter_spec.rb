require "spec_helper"
require "chef-vault"
require "chef-vault/test_fixtures"

describe "gitlab-exporters::redis_exporter" do
  include ChefVault::TestFixtures.rspec_shared_context

  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/redis_exporter").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("redis_exporter").with_options(
        password: "testingpassword"
      )
    end
  end

  context "wiht an alternative vault" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal["redis_exporter"]["chef_vault"] = "another-vault"
        node.normal["redis_exporter"]["chef_vault_item"] = "another-item"
      }.converge(described_recipe)
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("redis_exporter").with_options(
        password: "anothertestingpassword"
      )
    end
  end
end
