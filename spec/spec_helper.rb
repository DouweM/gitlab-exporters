require "chefspec"
require "chefspec/berkshelf"

RSpec.configure do |config|
  # Specify the path for Chef Solo to find cookbooks
  # config.cookbook_path = '/var/cookbooks'
  # Specify the path for Chef Solo to find roles
  config.role_path = "roles"
  # Specify the Chef log_level (default: :warn)
  # config.log_level = :debug
  # Specify the path to a local JSON file with Ohai data
  # config.path = 'ohai.json'
  # Specify the operating platform to mock Ohai data from
  config.platform = "ubuntu"
  # Specify the operating version to mock Ohai data from
  config.version = "16.04"
end

libraries_path = File.join(File.dirname(__FILE__), "..", "libraries")
$LOAD_PATH.unshift(libraries_path)

# As a way of setting a fixture
module Gitlab
  def self.private_ips_for_node(node)
    case node["hostname"]
    when "my.hostname"
      ["192.168.1.2"]
    when "labeled.hostname"
      ["192.168.1.3"]
    else
      []
    end
  end
end
